<?php

/**
 * Implementation of hook_views_default_views()
 */
function budget_views_default_views() {
  $view = new view;
  $view->name = 'Budget';
  $view->description = '';
  $view->tag = '';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'nid' => array(
      'label' => 'Nid',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 0,
	'path' => '',
	'link_class' => '',
	'alt' => '',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 0,
	'max_length' => '',
	'word_boundary' => 1,
	'ellipsis' => 1,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'link_to_node' => 0,
      'exclude' => 1,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'relationship' => 'none',
    ),
    'title' => array(
      'label' => 'Title',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 0,
	'path' => '',
	'link_class' => '',
	'alt' => '',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 0,
	'max_length' => '',
	'word_boundary' => 1,
	'ellipsis' => 1,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'link_to_node' => 1,
      'exclude' => 0,
      'id' => 'title',
      'table' => 'node',
      'field' => 'title',
      'relationship' => 'none',
    ),
    'created' => array(
      'label' => 'Post date',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 0,
	'path' => '',
	'link_class' => '',
	'alt' => '',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 0,
	'max_length' => '',
	'word_boundary' => 1,
	'ellipsis' => 1,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'date_format' => 'custom',
      'custom_date_format' => 'Y-m-d',
      'exclude' => 0,
      'id' => 'created',
      'table' => 'node',
      'field' => 'created',
      'relationship' => 'none',
    ),
    'field_reconciled_value' => array(
      'label' => 'Reconciled',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 1,
	'path' => 'budget/reconcile/[nid]',
	'link_class' => '',
	'alt' => 'Toggle Reconciled',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 1,
	'max_length' => '1',
	'word_boundary' => 0,
	'ellipsis' => 0,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'plain',
      'multiple' => array(
	'group' => TRUE,
	'multiple_number' => '',
	'multiple_from' => '',
	'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_reconciled_value',
      'table' => 'node_data_field_reconciled',
      'field' => 'field_reconciled_value',
      'relationship' => 'none',
    ),
    'field_description_value' => array(
      'label' => 'Description',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 0,
	'path' => '',
	'link_class' => '',
	'alt' => '',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 0,
	'max_length' => '',
	'word_boundary' => 1,
	'ellipsis' => 1,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
	'group' => TRUE,
	'multiple_number' => '',
	'multiple_from' => '',
	'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_description_value',
      'table' => 'node_data_field_description',
      'field' => 'field_description_value',
      'relationship' => 'none',
    ),
    'field_amount_value' => array(
      'label' => 'Amount',
      'alter' => array(
	'alter_text' => 0,
	'text' => '',
	'make_link' => 0,
	'path' => '',
	'link_class' => '',
	'alt' => '',
	'prefix' => '',
	'suffix' => '',
	'help' => '',
	'trim' => 0,
	'max_length' => '',
	'word_boundary' => 1,
	'ellipsis' => 1,
	'strip_tags' => 0,
	'html' => 0,
      ),
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
	'group' => TRUE,
	'multiple_number' => '',
	'multiple_from' => '',
	'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_amount_value',
      'table' => 'node_data_field_amount',
      'field' => 'field_amount_value',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'tid' => array(
      'default_action' => 'not found',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'fixed',
      'default_argument' => '',
      'validate_type' => 'none',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'add_table' => 0,
      'require_value' => 0,
      'reduce_duplicates' => 0,
      'set_breadcrumb' => 1,
      'id' => 'tid',
      'table' => 'term_node',
      'field' => 'tid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
	'2' => 0,
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_user' => 0,
      'default_argument_fixed' => '',
      'default_argument_php' => '',
      'validate_argument_node_type' => array(
	'page' => 0,
	'story' => 0,
	'transaction' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
	'1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_php' => '',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
	'transaction' => 'transaction',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
	'operator' => FALSE,
	'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'none',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('items_per_page', 25);
  $handler->override_option('use_pager', '1');
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 1,
    'order' => 'asc',
    'columns' => array(
      'title' => 'title',
      'created' => 'created',
      'field_reconciled_value' => 'field_reconciled_value',
      'field_description_value' => 'field_description_value',
      'field_amount_value' => 'field_amount_value',
    ),
    'info' => array(
      'title' => array(
	'sortable' => 1,
	'separator' => '',
      ),
      'created' => array(
	'sortable' => 1,
	'separator' => '',
      ),
      'field_reconciled_value' => array(
	'sortable' => 1,
	'separator' => '',
      ),
      'field_description_value' => array(
	'sortable' => 1,
	'separator' => '',
      ),
      'field_amount_value' => array(
	'sortable' => 1,
	'separator' => '',
      ),
    ),
    'default' => 'created',
  ));
  $handler = $view->new_display('page', 'Page', 'page_1');
  $handler->override_option('path', 'budget');
  $handler->override_option('menu', array(
    'type' => 'normal',
    'title' => 'Budget',
    'description' => '',
    'weight' => '0',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
  ));
  return $view;
}