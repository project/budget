<?php

function _budget_cck_export() {
  $content['type']  = array (
    'name' => 'Transaction',
    'type' => 'transaction',
    'description' => 'A transaction is a single financial record.  It records the amount of money spent, as well as what it was spent on, and any other information needed to properly identify it.',
    'title_label' => 'Payee',
    'body_label' => '',
    'min_word_count' => '0',
    'help' => '',
    'node_options' => 
    array (
      'status' => true,
      'promote' => false,
      'sticky' => false,
      'revision' => false,
    ),
    'old_type' => 'transaction',
    'orig_type' => '',
    'module' => 'node',
    'custom' => '1',
    'modified' => '1',
    'locked' => '1',
    'comment' => '0',
    'comment_default_mode' => '4',
    'comment_default_order' => '1',
    'comment_default_per_page' => '50',
    'comment_controls' => '3',
    'comment_anonymous' => 0,
    'comment_subject_field' => '1',
    'comment_preview' => '1',
    'comment_form_location' => '0',
  );
  $content['fields']  = array (
    0 => 
    array (
      'label' => 'Description',
      'field_name' => 'field_description',
      'type' => 'text',
      'widget_type' => 'text_textfield',
      'change' => 'Change basic information',
      'weight' => '-4',
      'rows' => 5,
      'size' => '60',
      'description' => '',
      'default_value' => 
      array (
	0 => 
	array (
	  'value' => '',
	  '_error_element' => 'default_value_widget][field_description][0][value',
	),
      ),
      'default_value_php' => '',
      'default_value_widget' => NULL,
      'group' => false,
      'required' => 0,
      'multiple' => '0',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'text',
      'columns' => 
      array (
	'value' => 
	array (
	  'type' => 'text',
	  'size' => 'big',
	  'not null' => false,
	  'sortable' => true,
	  'views' => true,
	),
      ),
      'display_settings' => 
      array (
	'weight' => '-4',
	'parent' => '',
	'label' => 
	array (
	  'format' => 'inline',
	),
	'teaser' => 
	array (
	  'format' => 'plain',
	  'exclude' => 0,
	),
	'full' => 
	array (
	  'format' => 'plain',
	  'exclude' => 0,
	),
	4 => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
      ),
    ),
    1 => 
    array (
      'label' => 'Amount',
      'field_name' => 'field_amount',
      'type' => 'number_decimal',
      'widget_type' => 'number',
      'change' => 'Change basic information',
      'weight' => '-3',
      'description' => 'The dollar value of the ',
      'default_value' => 
      array (
	0 => 
	array (
	  'value' => '',
	  '_error_element' => 'default_value_widget][field_amount][0][value',
	),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
	'field_amount' => 
	array (
	  0 => 
	  array (
	    'value' => '',
	    '_error_element' => 'default_value_widget][field_amount][0][value',
	  ),
	),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'precision' => '12',
      'scale' => '2',
      'decimal' => '.',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'number',
      'columns' => 
      array (
	'value' => 
	array (
	  'type' => 'numeric',
	  'precision' => '12',
	  'scale' => '2',
	  'not null' => false,
	  'sortable' => true,
	),
      ),
      'display_settings' => 
      array (
	'weight' => '-3',
	'parent' => '',
	'label' => 
	array (
	  'format' => 'inline',
	),
	'teaser' => 
	array (
	  'format' => 'us_0',
	  'exclude' => 0,
	),
	'full' => 
	array (
	  'format' => 'us_0',
	  'exclude' => 0,
	),
	4 => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
      ),
    ),
    2 => 
    array (
      'label' => 'Reconciled',
      'field_name' => 'field_reconciled',
      'type' => 'text',
      'widget_type' => 'optionwidgets_onoff',
      'change' => 'Change basic information',
      'weight' => '-2',
      'description' => '',
      'default_value' => 
      array (
	0 => 
	array (
	  'value' => NULL,
	),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
	'field_reconciled' => 
	array (
	  'value' => false,
	),
      ),
      'group' => false,
      'required' => 1,
      'multiple' => '0',
      'text_processing' => '0',
      'max_length' => '',
      'allowed_values' => 'Unreconciled
  Reconciled',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'text',
      'widget_module' => 'optionwidgets',
      'columns' => 
      array (
	'value' => 
	array (
	  'type' => 'text',
	  'size' => 'big',
	  'not null' => false,
	  'sortable' => true,
	  'views' => true,
	),
      ),
      'display_settings' => 
      array (
	'weight' => '-2',
	'parent' => '',
	'label' => 
	array (
	  'format' => 'inline',
	),
	'teaser' => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
	'full' => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
	4 => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
      ),
    ),
    3 => 
    array (
      'label' => 'Transaction ID',
      'field_name' => 'field_transaction_id',
      'type' => 'number_integer',
      'widget_type' => 'number',
      'change' => 'Change basic information',
      'weight' => '-1',
      'description' => '',
      'default_value' => 
      array (
	0 => 
	array (
	  'value' => '',
	  '_error_element' => 'default_value_widget][field_transaction_id][0][value',
	),
      ),
      'default_value_php' => '',
      'default_value_widget' => 
      array (
	'field_transaction_id' => 
	array (
	  0 => 
	  array (
	    'value' => '',
	    '_error_element' => 'default_value_widget][field_transaction_id][0][value',
	  ),
	),
      ),
      'group' => false,
      'required' => 0,
      'multiple' => '0',
      'min' => '',
      'max' => '',
      'prefix' => '',
      'suffix' => '',
      'allowed_values' => '',
      'allowed_values_php' => '',
      'op' => 'Save field settings',
      'module' => 'number',
      'widget_module' => 'number',
      'columns' => 
      array (
	'value' => 
	array (
	  'type' => 'int',
	  'not null' => false,
	  'sortable' => true,
	),
      ),
      'display_settings' => 
      array (
	'weight' => '-1',
	'parent' => '',
	'label' => 
	array (
	  'format' => 'hidden',
	),
	'teaser' => 
	array (
	  'format' => 'hidden',
	  'exclude' => 0,
	),
	'full' => 
	array (
	  'format' => 'hidden',
	  'exclude' => 0,
	),
	4 => 
	array (
	  'format' => 'default',
	  'exclude' => 0,
	),
      ),
    ),
  );
  $content['extra']  = array (
    'title' => '-5',
    'revision_information' => '2',
    'comment_settings' => '3',
    'menu' => '1',
    'taxonomy' => '0',
  );
  return $content;
}
